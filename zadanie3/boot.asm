[bits 16]
[org 0x7c00]

jmp 0:main

;;;

PROMPT: db 'Enter your name', 0xd, 0xa, 0x0
WELCOME: db 'Hello ', 0x0
NEWLINE: db 0xd, 0xa, 0x0

NAME: times 13 db 0

;;;

freeze_2_seconds:
    ; set timer to 2 seconds
    mov cx, 0x001E
    mov dx, 0x8480

    ; wait
    mov ah, 0x86
    int 0x15

    ret

;;;

print_str:
    ; check for 0x0
    mov al, [si]
    test al, al
    je end_print

    ; print char
    mov ah, 0xe
    int 0x10

    ; move pointer
    inc si

    ; loop
    jmp print_str

end_print:
    ret

;;;

get_name:
    ; move buffer address
    mov bx, NAME

    ; initialize counter
    mov cx, 0

get_char:
    ; get keystroke
    mov ah, 0x0
    int 0x16

    ; check if enter
    cmp al, 0xd
    je end_get

    ; check if backspace
    cmp al, 0x8
    je backspace

    ; check if name too long
    cmp cx, 12
    je get_char

    ; update counter
    add cx, 1

    ;print letter
    mov ah, 0xe
    int 0x10

    ;save letter
    mov [bx], al
    inc bx
    jne get_char

backspace:
    ; check if empty
    cmp cx, 0
    je get_char

    ; erase char from name
    mov [bx], byte 0
    dec bx

    ; update counter
    sub cx, 1

    ; erase char from screen
    mov ah, 0xe

    mov al, 0x8
    int 0x10

    mov al, 0x0
    int 0x10

    mov al, 0x8
    int 0x10

    jmp get_char

end_get:
    ; check if name too short
    cmp cx, 2
    jle get_char

    ret

;;;

main:
    ; set stack etc.
    mov ax, cs
    mov ds, ax
    mov es, ax
    mov ss, ax
    mov sp, 0x8000

    ; prompt user to input name
    mov si, PROMPT
    call print_str

    call get_name

    mov si, NEWLINE
    call print_str

    mov si, WELCOME
    call print_str

    mov si, NAME
    call print_str

    mov si, NEWLINE
    call print_str

    ; save name in 4th sector, cx: c
    xor ax, ax
    mov es, ax ; just in case
    mov cx, 0x0004 ; cylinder : sector
    mov dl, 0x80 ; disc
    mov bx, NAME ; buffer
    mov al, 0x01 ; times
    mov ah, 0x03 ; write to sector
    int 13h ; interrupt

    ; wait 2 seconds
    call freeze_2_seconds

    ; load bootloader loader
    mov al, 0x01
    mov bx, 0x7e00
    mov cx, 0x0002
    mov dl, 0x80
    xor dh, dh
    mov ah, 0x02 ; read from sector
    int 0x13
    jmp 0x7e00

;;;

times 510 - ($ - $$) db 0
dw 0xAA55

;;;

original_bootloader_caller:
    mov al, 0x01
    mov bx, 0x7c00
    mov cx, 0x0003
    mov dl, 0x80
    xor dh, dh
    mov ah, 0x02
    int 0x13
    jmp 0x7c00

;;;

times 1022 - ($ - $$) db 0
dw 0xAA55

;;;

original_bootloader:
    incbin "org_boot" ; copy contents to this file
