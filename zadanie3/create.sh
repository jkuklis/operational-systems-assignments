#!/bin/bash

#new bootloader will be in 'file'

nasm -f bin boot.asm -o cus_file
dd if=cus_file of=file
dd conv=notrunc bs=512 count=1 if=org_boot of=file
dd conv=notrunc bs=444 count=1 if=cus_file of=file
#scp file jk371125@students.mimuw.edu.pl:~/so
