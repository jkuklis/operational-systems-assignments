#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

extern void *__libc_stack_end;
extern uint8_t __executable_start;
extern uint8_t __etext;

void stack_show() {

    uint8_t local_var;
    void *stack_ptr = &local_var;

    while (stack_ptr < __libc_stack_end) {

        stack_ptr ++;

        uint64_t stack_ptr_value = *((uint64_t *)stack_ptr);

        uint8_t *stack_ptr_casted = (uint8_t *)(stack_ptr_value);

        int after_start = &__executable_start <= stack_ptr_casted;
        int before_end = stack_ptr_casted < &__etext;

        if (after_start && before_end) {

            uint8_t *e8_search_ptr = stack_ptr_casted;

            e8_search_ptr -= 5;

            if (*e8_search_ptr == (uint8_t)0xe8) {

                uint8_t *after_e8_ptr = e8_search_ptr + 1;

                int32_t rel_address_32 = *((int32_t*)after_e8_ptr);

                uint64_t address = (uint64_t)(stack_ptr_value + (int64_t)(rel_address_32));

                printf("%.16lx\n", address);

            }
        }
    }
}
