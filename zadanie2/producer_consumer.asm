section .text

global init, producer, consumer
extern produce, consume
extern proberen, verhogen
extern malloc

init:
    ; stack pointer
    sub     rsp, 8

    ; move function argument to r8
    mov     [buffer_size], rdi
    ; move upper limit to r9
    mov     rdx, 0x80000000

    ; check upper limit
    cmp     [buffer_size], rdx
    jge     end1

    ; check if 0
    cmp     qword[buffer_size], 0
    je      end2

    ; init semaphores
    mov     dword[put_sem], edi
    mov     dword[take_sem], 0

    ; allocate [rdi * 8] bytes
    imul    rdi, 8
    call    malloc

    ; check, if successful
    test    rax, rax
    jz      end3

    ; save array_ptr
    mov     [array_ptr], rax

end0:
    mov     eax, 0
    add     rsp, 8
    ret

end1:
    mov     eax, -1
    add     rsp, 8
    ret

end2:
    mov     eax, -2
    add     rsp, 8
    ret

end3:
    mov     eax, -3
    add     rsp, 8
    ret


producer:
    ; stack pointer
    sub     rsp, 8

producer_loop:
    mov     qword[to_put], 0

producer_repeat:

    ; produce portion
    mov     rdi, prod_port
    call    produce

    ; if returned 0, exit
    cmp     rax, 0
    je      end_prod

    ; get semaphore
    mov     rdi, put_sem
    call    proberen

    ; place portion
    mov     rdx, qword[prod_port]
    mov     rax, qword[to_put]
    mov     rcx, qword[array_ptr]
    mov     [rcx + rax * 8], rdx

    ; release semaphore
    mov     rdi, take_sem
    call    verhogen

    ; update counter
    add     qword[to_put], 1
    mov     rdx, qword[buffer_size]
    cmp     qword[to_put], rdx
    je      producer_loop

    ; loop
    jmp       producer_repeat

end_prod:
    add     rsp, 8
    ret



consumer:
    ; stack pointer
    sub     rsp, 8

consumer_loop:
    mov     qword[to_take], 0

consumer_repeat:
    ; get semaphore
    mov     rdi, take_sem
    call    proberen

    ; take portion
    mov     rax, qword[to_take]
    mov     rcx, qword[array_ptr]
    mov     rdx, qword[rcx + rax * 8]
    mov     [cons_port], rdx

    ; release semaphore
    mov     rdi, put_sem
    call    verhogen

    ; consume portion
    mov     rdi, [cons_port]
    call    consume

    ; if returend 0, exit
    cmp     rax, 0
    je      end_cons

    ; update counter
    add     qword[to_take], 1
    mov     rdx, qword[buffer_size]
    cmp     qword[to_take], rdx
    je      consumer_loop

    ; loop
    jmp     consumer_repeat

end_cons:
    add     rsp, 8
    ret


section .bss
    buffer_size: resb 8
    array_ptr: resb 8
    put_sem: resb 8
    take_sem: resb 8
    to_put: resb 8
    to_take: resb 8
    prod_port: resb 8
    cons_port: resb 8
