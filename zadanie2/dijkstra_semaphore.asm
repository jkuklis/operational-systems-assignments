section .text

global proberen, verhogen

proberen:
    ; move function argument
    mov         rdx, rdi
    jmp         prob_loop

prob_restore:
    ; restore valid state
    mov         eax, 1
    lock xadd   DWORD[rdx], eax

prob_loop:
    ; avoid deadlock
    cmp         DWORD[rdx], 0
    jle         prob_loop

    ; try to acquire
    mov         eax, -1
    lock xadd   DWORD[rdx], eax
    cmp         eax, 1
    jl          prob_restore

    ret

verhogen:
    ; move function argument
    mov         rdx, rdi

    ; lift semaphore
    mov         eax, 1
    lock xadd   DWORD[rdx], eax

    ret
