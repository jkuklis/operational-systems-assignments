pre='/usr/src/minix/'
ser='servers/pm/'

cp -f callnr.h $pre'include/minix/'
cp -f callnr.h '/usr/include/minix/'
cp -f proto.h $pre$ser
cp -f table.c $pre$ser
cp -f Makefile $pre$ser
cp do_myps.c $pre$ser
cp -f unistd.h '/usr/include/'
cp -f unistd.h '/usr/src/include/'
cp myps.c '/usr/src/lib/libc/misc/'
cp -f Makefile.inc '/usr/src/lib/libc/misc/'
make -C /usr/src/minix/servers/pm/
make -C /usr/src/minix/servers/pm/ install
make -C /usr/src/lib/libc/
make -C /usr/src/lib/libc/ install
make -C /usr/src/releasetools/ hdboot
