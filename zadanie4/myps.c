#include <lib.h>      // provides _syscall and message
#include <unistd.h>   // provides function prototype (see step 1 below)

int myps(int uid) {
    message m;      // Minix message to pass parameters to a system call

    m.m1_i1 = uid;  // set first integer of message to val

    return _syscall(PM_PROC_NR, PM_MYPS, &m);  // invoke underlying system call
}
