#include <lib.h>    // provides _syscall and message
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    if (argc < 2)
        exit(1);

    int i = atoi(argv[1]);

    message m;  // Minix uses message to pass parameters to a system call

    m.m1_i1 = i;

    _syscall(PM_PROC_NR, PM_MYPS, &m);
        /* _syscall leads to the system server identified by PM_PRO_NR (the PM
         * server process) invoking the function identified by call number
         * PRINTMSG with parameters in the message copied to address &m
         */
}
