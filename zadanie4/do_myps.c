#include <lib.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <minix/rs.h>
#include <minix/config.h>
#include <minix/callnr.h>
#include <minix/com.h>

#include "pm.h"
#include "mproc.h"

int do_myps(void) {
  
    int uid = m_in.m1_i1;
    int i = 0;

    struct mproc *rmp = mp;
    
    if (uid == 0) {
      uid = (int) rmp->mp_realuid;
    }

    while (i < NR_PROCS) {
        uid_t proc_uid = mproc[i].mp_realuid;
        
        if (uid == proc_uid) {
            int pid = mproc[i].mp_pid;
            if (pid != 0) {
                int ppid = mproc[mproc[i].mp_parent].mp_pid;
                printf("pid: %d, ppid: %d, uid: %u\n", pid, ppid, uid);
            }
        }

        i++;
    }
    //printf("Hi\n");
    return 0;
}
